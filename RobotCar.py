#RC.py
#Robot Car 
import sys
import time
import RPi.GPIO as GPIO
try :
    dist,select,pos = 0.0,0,0
    #motor driver
    in1motorA = 29
    in2motorA = 31
    in1motorB = 33
    in2motorB = 35
    #set GPIO Pins
    TRIGGER = 12
    ECHO = 18
    GPIO.setmode(GPIO.BOARD)
    #set GPIO direction (IN / OUT)
    GPIO.setup(3, GPIO.OUT)
    GPIO.setup(TRIGGER, GPIO.OUT)
    GPIO.setup(ECHO, GPIO.IN)
    GPIO.setup(in1motorA,GPIO.OUT)
    GPIO.setup(in2motorA,GPIO.OUT)
    GPIO.setup(in1motorB,GPIO.OUT)
    GPIO.setup(in2motorB,GPIO.OUT)
    GPIO.output(in1motorA,GPIO.LOW)
    GPIO.output(in2motorA,GPIO.LOW)
    GPIO.output(in1motorB,GPIO.LOW)
    GPIO.output(in2motorB,GPIO.LOW)
    
    pwm = GPIO.PWM(3,50)
    pwm.start(7.5)

    def SetAngle(angle):
        
            duty = angle /18 +2.5
            GPIO.output(3, True)
            pwm.ChangeDutyCycle(duty)
            time.sleep(1)
            GPIO.output(3, False)
            pwm.ChangeDutyCycle(0)
    
    def forword():
        
        GPIO.output(in1motorA,GPIO.HIGH)
        GPIO.output(in2motorA,GPIO.LOW)
        GPIO.output(in1motorB,GPIO.HIGH)
        GPIO.output(in2motorB,GPIO.LOW)   
         
    def back():
        GPIO.output(in1motorA,GPIO.LOW)
        GPIO.output(in2motorA,GPIO.HIGH)
        GPIO.output(in1motorB,GPIO.LOW)
        GPIO.output(in2motorB,GPIO.HIGH)   
        print("back")
    def left():
        GPIO.output(in1motorA,GPIO.LOW)
        GPIO.output(in2motorA,GPIO.HIGH)
        GPIO.output(in1motorB,GPIO.HIGH)
        GPIO.output(in2motorB,GPIO.LOW)   
        print("left")
    def rihgt():
        GPIO.output(in1motorA,GPIO.HIGH)
        GPIO.output(in2motorA,GPIO.LOW)
        GPIO.output(in1motorB,GPIO.LOW)
        GPIO.output(in2motorB,GPIO.HIGH)   
        print("rihgt")
    def stop():
        GPIO.output(in1motorA,GPIO.LOW)
        GPIO.output(in2motorA,GPIO.LOW)
        GPIO.output(in1motorB,GPIO.LOW)
        GPIO.output(in2motorB,GPIO.LOW)   
        print("stop")
    def test():
        forword()
        time.sleep(1)
        stop()
        time.sleep(1)
        back()
        time.sleep(1)
        stop()
        time.sleep(1)
        rihgt()
        time.sleep(1)
        stop()
        time.sleep(1)
        left()
        time.sleep(1)
        stop()
        time.sleep(1)
   
    def distance():
        # set Trigger to HIGH
        GPIO.output(TRIGGER, True)
 
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(TRIGGER, False)
 
        StartTime = time.time()
        StopTime = time.time()
 
        # save StartTime
        while GPIO.input(ECHO) == 0:
            StartTime = time.time()
 
        # save time of arrival
        while GPIO.input(ECHO) == 1:
            StopTime = time.time()
 
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
 
        return distance

    #test()
    while True:
        dist = distance()
        print ("Measured Distance = %.1f cm" % dist)
        SetAngle(90)
        print ("dg 90" )
        while dist > 20:
          forword()
          dist = distance()
          print ("Measured Distance forword= %.1f cm" % dist)
        while dist <= 20:
            if (pos >= 255):
                pos = 0
                dist = distance()
                print ("if 255 = %.1f cm" % dist) 
            stop()
            time.sleep(1)
            print ("post= %.1f cm" % pos)
            SetAngle(pos)
            select = pos
            pos += 45
            dist = distance()
            print ("Measured Distance <20= %.1f cm" % dist)
            
        SetAngle(90)
        if (select == 0):
            rihgt()
            time.sleep(0.25)
            print ("Measured Distance 90= %.1f cm" % dist)
            stop()
            
        elif (select == 45):
            rihgt()
            time.sleep(0.5)
            print ("Measured Distance 45= %.1f cm" % dist)
            stop()
            
        elif (select == 135):
            left()
            time.sleep(0.25)
            print ("Measured Distance 135= %.1f cm" % dist)
            stop()
            
        elif (select == 180):
            left()
            time.sleep(0.5)
            print ("Measured Distance 180 = %.1f cm" % dist)
            stop()
           
            
            
            
    
except KeyboardInterrupt:
     print("you exited the program")
finally:
           SetAngle(90)
           print ("dg 90" )
           pwm.stop()
           GPIO.cleanup()
        
        
