# README #

The goal of the project is to create a simple 4-wheeled Raspberry Pi Robot Car.
The process for completing this is to connect motors, a Raspberry Pi,
and a battery pack to a L298n H-bridge motor board through wires,
place that circuit in a designed car frame, and to program the raspberry Pi with Python.

